using cis.domain.entities;
using cis.http.DTOs.request;
using cis.http.DTOs.response;
using cis.infra.database.mysql.repositories.topics;
using cis.services;
using Moq;
using Xunit;

namespace cis.tests.services;

public class TopicsServiceTest
{
    [Fact(DisplayName = "Adding Topic")]
    public void AddTopic()
    {
        var mockRepository = new Mock<ITopicRepository>();
        var topicService = new TopicsService(mockRepository.Object);
        var requestDto = new TopicRequestDTo("Clean Code");
        var userLogin = "user@gmail.com";
        
        topicService.AddTopics(requestDto, userLogin);
        
        mockRepository.Verify(repo => repo.AddTopic(It.IsAny<Topics>()), Times.Once);
    }
    
    [Fact(DisplayName = "Get all Topics")]
    public void GetAllTopics()
    {
        List<Topics> topics = new List<Topics>();
        topics.Add(new Topics("user@gmail.com","Clean Code"));
        topics.Add(new Topics("use@gmail.com","Minecraft"));
        topics.Add(new Topics("userr@gmail.com","MongoDB"));
        
        var mockRepository = new Mock<ITopicRepository>();
        mockRepository.Setup(repo => repo.GetAllTopics()).Returns(topics);
        var topicService = new TopicsService(mockRepository.Object);
        var result = topicService.GetAllTopics();
        Assert.NotNull(result);
        Assert.IsType<List<TopicResponseDTo>>(result);
    }
    
    [Fact(DisplayName = "Get Topic by id")]
    public void GetTopicByID()
    {
        List<Topics> topics = new List<Topics>();
        topics.Add(new Topics("user@gmail.com","Clean Code"));
        topics.Add(new Topics("use@gmail.com","Minecraft"));
        topics.Add(new Topics("userr@gmail.com","MongoDB"));
        
        var mockRepository = new Mock<ITopicRepository>();
        mockRepository.Setup(repo => repo.GetTopicsById(topics[0].Id)).Returns(topics[0]);
        
        var topicService = new TopicsService(mockRepository.Object);
        string topicId = topics[0].Id;
        
        var result = topicService.GetTopicById(topicId);
        
        Assert.NotNull(result);
        Assert.IsType<TopicResponseDTo>(result);
        Assert.Equal(topicId, result.Id);
    }
}