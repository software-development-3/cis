using cis.domain.entities;
using cis.http.DTOs.request;
using cis.infra.database.mysql.repositories.votes;
using cis.services;
using Moq;
using Xunit;

namespace cis.tests.services;

public class VotesServiceTest
{
    [Fact(DisplayName = "Add Votes for Idea")]
    public void AddVotes()
    {
        var mockRepository = new Mock<IVotesRepository>();
        var voteService = new VotesService(mockRepository.Object);
        var requestDto = new RequestVoteDTo(Guid.NewGuid().ToString());
        var userLogin = "user@gmail.com";
        
        voteService.AddVote(requestDto,userLogin);
        
        mockRepository.Verify(repo => repo.AddVotes(It.IsAny<Votes>()), Times.Once);
    }
}