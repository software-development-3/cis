using cis.domain.entities;
using cis.http.DTOs.request;
using cis.http.DTOs.response;
using cis.infra.database.mysql.repositories.Ideas;
using cis.services;
using Moq;
using Xunit;

namespace cis.tests.services;

public class IdeaServiceTest
{
    [Fact(DisplayName = "Adding Idea")]
    public void AddIdea()
    {
        var mockRepository = new Mock<IIdeasRepository>();
        var ideaService = new IdeaService(mockRepository.Object);
        var requestDto = new RequestIdeaDTo("Clean Code", "This idea is about clean code book",Guid.NewGuid().ToString());
        var userLogin = "user@gmail.com";
        
        ideaService.AddIdea(requestDto, userLogin);
        
        mockRepository.Verify(repo => repo.AddIdeas(It.IsAny<Idea>()), Times.Once);
    }
    
    [Fact(DisplayName = "Get all Ideas")]
    public void GetAllIdea()
    {
        List<Idea> ideas = new List<Idea>();
        ideas.Add(new Idea("Clean code","Book","user@gmail.com",Guid.NewGuid().ToString()));
        ideas.Add(new Idea("The best code","Codes","userd@gmail.com",Guid.NewGuid().ToString()));
        
        var mockRepository = new Mock<IIdeasRepository>();
        mockRepository.Setup(repo => repo.GetAllIdeas()).Returns(ideas);
        var ideaService = new IdeaService(mockRepository.Object);
        var result = ideaService.GetAllIdeas();
        Assert.NotNull(result);
        Assert.IsType<List<ResponseIdeaDTo>>(result);
    }
    
    [Fact(DisplayName = "Get Idea by id")]
    public void GetIdeaByID()
    {
        List<Idea> ideas = new List<Idea>();
        ideas.Add(new Idea("Clean code","Book","user@gmail.com",Guid.NewGuid().ToString()));
        ideas.Add(new Idea("The best code","Codes","userd@gmail.com",Guid.NewGuid().ToString()));
        
        var mockRepository = new Mock<IIdeasRepository>();
        mockRepository.Setup(repo => repo.GetIdeaById(ideas[0].Id)).Returns(ideas[0]);
        
        var ideaService = new IdeaService(mockRepository.Object);
        string ideaId = ideas[0].Id;
        
        var result = ideaService.GetIdeaById(ideaId);
        
        Assert.NotNull(result);
        Assert.IsType<ResponseIdeaDTo>(result);
        Assert.Equal(ideaId, result.Id);
    }
    
    [Fact(DisplayName = "Delete Idea")]
    public void DeleteIdeaByID()
    {
        List<Idea> ideas = new List<Idea>();
        ideas.Add(new Idea("Clean code","Book","user@gmail.com",Guid.NewGuid().ToString()));
        ideas.Add(new Idea("The best code","Codes","userd@gmail.com",Guid.NewGuid().ToString()));
        
        var mockRepository = new Mock<IIdeasRepository>();
        
        mockRepository.Setup(repo => repo.DeleteIdeaById(It.IsAny<string>()))
            .Callback<string>((id) => ideas.RemoveAll(i => i.Id == id));
        
        var ideaService = new IdeaService(mockRepository.Object);
        
        string ideaId = ideas[0].Id;
        ideaService.DeleteIdea(ideaId);
        Assert.DoesNotContain(ideas, i => i.Id == ideaId);
    }
    
    [Fact(DisplayName = "Update Idea")]
    public void UpdateIdea()
    {
        List<Idea> ideas = new List<Idea>();
        ideas.Add(new Idea("Clean code","Book","user@gmail.com",Guid.NewGuid().ToString()));
        ideas.Add(new Idea("The best code","Codes","userd@gmail.com",Guid.NewGuid().ToString()));
        
        Idea olderIdea = ideas[0];
        RequestIdeaDTo ideaEdit = new RequestIdeaDTo("Clean code 2", "Book", Guid.NewGuid().ToString());
        
        var mockRepository = new Mock<IIdeasRepository>();
        
        mockRepository.Setup(repo => repo.EditIdea(It.IsAny<string>(), It.IsAny<Idea>()))
            .Callback<string, Idea>((id, newIdea) =>
            {
                var index = ideas.FindIndex(i => i.Id == id);
                if (index >= 0)
                {
                    ideas[index].Title = newIdea.Title;
                    ideas[index].Description = newIdea.Description;
                    ideas[index].TopicId = newIdea.TopicId;
                }
            });
        
        var ideaService = new IdeaService(mockRepository.Object);
        
        ideaService.UpdateIdea(olderIdea.Id, ideaEdit, olderIdea.UserLogin);
        
        Assert.Equal(ideaEdit.Title, ideas[0].Title);
        Assert.Equal(ideaEdit.Description, ideas[0].Description);
        Assert.Equal(olderIdea.UserLogin, ideas[0].UserLogin);
    }
}