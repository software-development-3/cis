﻿namespace cis.http.DTOs.response;

public record TopicResponseDTo(string Id, string Title);