namespace cis.http.DTOs.response;

public record ResponseIdeaDTo(string Id, string Title, string Description, string UserLogin, string TopicId);