﻿namespace cis.http.DTOs.request;

public record TopicRequestDTo(string Title);