namespace cis.http.DTOs.request;

public record RequestIdeaDTo(string Title, string Description, string TopicId);