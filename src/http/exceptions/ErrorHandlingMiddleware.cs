using System.Net;
using System.Text.Json;
using Microsoft.AspNetCore.Diagnostics;

namespace cis.http.exceptions;

public class ErrorHandlingMiddleware : IExceptionHandler
{
    private readonly ILogger<ErrorHandlingMiddleware> logger;
 
    public ErrorHandlingMiddleware(ILogger<ErrorHandlingMiddleware> logger)
    {
        this.logger = logger;

    }
    
    public ValueTask<bool> TryHandleAsync(HttpContext httpContext, Exception exception, CancellationToken cancellationToken)
    {
        var exceptionMessage = exception.Message;
        logger.LogError(
            "Error Message: {exceptionMessage}, Time of occurrence {time}",
            exceptionMessage, DateTime.UtcNow);
        // Return false to continue with the default behavior
        // - or - return true to signal that this exception is handled
        return ValueTask.FromResult(false);
    }
}