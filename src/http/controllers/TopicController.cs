﻿using cis.http.DTOs.request;
using cis.services;
using Microsoft.AspNetCore.Mvc;

namespace cis.http.controllers;

[ApiController]
[Route("cis/topics/[controller]")]
public class TopicController : ControllerBase
{
    private readonly TopicsService _topicsService = new(null);

    [HttpPost]
    public ActionResult AddTopic([FromBody] TopicRequestDTo dTo)
    {
        var userLogin = HttpContext.Items["login"] as string;

        if (userLogin != null) _topicsService.AddTopics(dTo, userLogin);

        return Created();
    }

    [HttpGet]
    public ActionResult GetAllTopics()
    {
        return Ok(_topicsService.GetAllTopics());
    }

    [HttpGet("{id}")]
    public ActionResult GetTopicById([FromRoute] string id)
    {
        return Ok(_topicsService.GetTopicById(id));
    }
}