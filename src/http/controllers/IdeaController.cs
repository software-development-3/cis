using cis.http.DTOs.request;
using cis.services;
using Microsoft.AspNetCore.Mvc;

namespace cis.http.controllers;

[ApiController]
[Route("cis/ideas/[controller]")]
public class IdeaController : ControllerBase
{
    private readonly IdeaService _ideaService = new(null);

    [HttpPost]
    public ActionResult AddIdea([FromBody] RequestIdeaDTo dto)
    {
        var userLogin = HttpContext.Items["login"] as string;

        if (userLogin != null) _ideaService.AddIdea(dto, userLogin);
        return Created();
    }

    [HttpGet]
    public ActionResult GetAllIdeas()
    {
        return Ok(_ideaService.GetAllIdeas());
    }

    [HttpGet("{id}")]
    public ActionResult GetIdeaByID([FromRoute] string id)
    {
        return Ok((_ideaService.GetIdeaById(id)));
    }

    [HttpDelete("{id}")]
    public ActionResult DeleteIdea([FromRoute] string id)
    {
        _ideaService.DeleteIdea(id);
        return Ok("Idea deleted successfully");
    }

    [HttpPut("{id}")]
    public ActionResult UpdateIdea([FromBody] RequestIdeaDTo dto, [FromRoute] string id)
    {
        var userLogin = HttpContext.Items["login"] as string;

        if (userLogin != null) _ideaService.UpdateIdea(id, dto, userLogin);
        return Ok("Idea successfully updated");
    }
}