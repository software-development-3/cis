﻿using cis.http.DTOs.request;
using cis.services;
using Microsoft.AspNetCore.Mvc;

namespace cis.http.controllers;

[ApiController]
[Route("cis/vote/[controller]")]
public class VoteController : ControllerBase
{
    private readonly VotesService _votesService = new(null);
    
    [HttpPost]
    public ActionResult AddVote([FromBody] RequestVoteDTo dTo)
    {
        var userLogin = HttpContext.Items["login"] as string;


        if (userLogin != null) _votesService.AddVote(dTo, userLogin);

        return Created();
    }
}