﻿using cis.domain.entities;
using cis.http.DTOs.request;
using cis.infra.database.mysql.repositories.votes;

namespace cis.services;

public class VotesService
{
    private readonly IVotesRepository _votesRepository;

    public VotesService(IVotesRepository? votesRepository)
    {
        _votesRepository = votesRepository ?? new VotesRepository();
    }

    public void AddVote(RequestVoteDTo requestVoteDTo, string userLogin)
    {
        var vote = new Votes(userLogin, requestVoteDTo.IdeaId);
        
        _votesRepository.AddVotes(vote);
    }
}