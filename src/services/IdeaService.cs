using cis.domain.entities;
using cis.domain.factory;
using cis.http.DTOs.request;
using cis.http.DTOs.response;
using cis.infra.database.mysql.repositories.Ideas;

namespace cis.services;

public class IdeaService
{
    private readonly IIdeasRepository _ideasRepository;

    public IdeaService(IIdeasRepository? ideasRepository)
    {
        _ideasRepository = ideasRepository ?? new IdeasRepository();
    }

    public void AddIdea(RequestIdeaDTo dto, string userLogin)
    {
       _ideasRepository.AddIdeas(IdeaFactory.Add(dto, userLogin));
    }

    public List<ResponseIdeaDTo> GetAllIdeas()
    {
        return IdeaFactory.getAllIdeas(_ideasRepository.GetAllIdeas());
    }

    public ResponseIdeaDTo GetIdeaById(string id)
    {
        var idea = _ideasRepository.GetIdeaById(id);
        
        return new ResponseIdeaDTo(idea.Id, idea.Title, idea.Description, idea.UserLogin, idea.TopicId);
    }

    public void DeleteIdea(string id)
    {
        _ideasRepository.DeleteIdeaById(id);
    }

    public void UpdateIdea(string id, RequestIdeaDTo dto, string userLogin)
    {
        Idea newIdea = IdeaFactory.Add(dto, userLogin);
        _ideasRepository.EditIdea(id, newIdea);
    }
}