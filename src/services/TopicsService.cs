﻿using cis.domain.factory;
using cis.http.DTOs.request;
using cis.http.DTOs.response;
using cis.infra.database.mysql.repositories.topics;

namespace cis.services;

public class TopicsService
{
    private readonly ITopicRepository _topicRepository;

    public TopicsService(ITopicRepository? topicRepository)
    {
        _topicRepository = topicRepository ?? new TopicRepository();
    }

    public void AddTopics(TopicRequestDTo dTo, string userId)
    {
        _topicRepository.AddTopic(TopicFactory.Add(dTo, userId));
    }

    public List<TopicResponseDTo> GetAllTopics()
    {
        return TopicFactory.GetAllTopics(_topicRepository.GetAllTopics());
    }
    
    public TopicResponseDTo GetTopicById(string id)
    {
        var topic = _topicRepository.GetTopicsById(id);
        
        return new TopicResponseDTo(topic.Id, topic.Title);
    }
}