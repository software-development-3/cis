﻿using cis.domain.entities;
using cis.http.DTOs.request;
using cis.http.DTOs.response;

namespace cis.domain.factory;

public class TopicFactory
{
    public static Topics Add(TopicRequestDTo requestDTo, string userLogin)
    {
        return new Topics(userLogin, requestDTo.Title);
    }

    public static List<TopicResponseDTo> GetAllTopics(List<Topics> topics)
    {
        return topics.Select(topic => new TopicResponseDTo(topic.Id, topic.Title)).ToList();
    }
}