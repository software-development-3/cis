using System.Net.Sockets;
using cis.domain.entities;
using cis.http.DTOs.request;
using cis.http.DTOs.response;

namespace cis.domain.factory;

public class IdeaFactory
{
    public static Idea Add(RequestIdeaDTo dTo, string userLogin)
    {
        return new Idea(dTo.Title,dTo.Description, userLogin, dTo.TopicId);
    }

    public static List<ResponseIdeaDTo> getAllIdeas(List<Idea> ideas)
    {
        return ideas.Select(idea => new ResponseIdeaDTo(idea.Id, idea.Title, idea.Description, idea.UserLogin, idea.TopicId)).ToList();
    }
}