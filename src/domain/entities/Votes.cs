using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace cis.domain.entities;

[BsonIgnoreExtraElements]
public class Votes
{
    public Votes(string userLogin, string ideaId)
    {
        Id = Guid.NewGuid().ToString();
        UserLogin = userLogin;
        IdeaId = ideaId;
    }
    
    [BsonId]
    [BsonRepresentation(BsonType.String)]
    [BsonElement("_id")]
    public string Id { get; set; }
    [BsonElement("user_login"), MinLength(36), MaxLength(36)]
    public string UserLogin { get; set; }
    [BsonElement("idea_id"), MinLength(36), MaxLength(36)]
    public string IdeaId { get; set; }
}
