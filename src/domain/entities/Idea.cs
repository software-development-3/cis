using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace cis.domain.entities;

[BsonIgnoreExtraElements]
public class Idea
{
    public Idea(string title, string description, string userLogin, string topicId)
    {
        Id = Guid.NewGuid().ToString();
        Title = title;
        Description = description;
        UserLogin = userLogin;
        TopicId = topicId;
    }
    
    [BsonId]
    [BsonRepresentation(BsonType.String)]
    [BsonElement("_id")]
    public string Id { get; set; }

    [BsonElement("title")]
    public string Title { get; set; }

    [BsonElement("description")]
    public string Description { get; set; }

    [BsonElement("userLogin")]
    public string UserLogin { get; set; }

    [BsonElement("topicId")]
    public string TopicId { get; set; }
}
