using MongoDB.Bson.Serialization.Attributes;
using System;
using MongoDB.Bson;

namespace cis.domain.entities
{
    [BsonIgnoreExtraElements]
    public class Topics
    {
        [BsonId]
        [BsonRepresentation(BsonType.String)]
        [BsonElement("_id")]
        public string Id { get; set; }

        [BsonElement("userLogin")]
        public string UserLogin { get; set; }

        [BsonElement("title")]
        public string Title { get; set; }

        public Topics(string userLogin, string title)
        {
            Id = Guid.NewGuid().ToString();
            UserLogin = userLogin;
            Title = title;
        }
    }
}