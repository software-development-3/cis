using cis.domain.entities;
using Microsoft.EntityFrameworkCore;
using MongoDB.Driver;

namespace cis.infra.database.mysql;

public class AppDbContext
{
    public IMongoCollection<Votes> Votes { get; private set; }
    public IMongoCollection<Idea> Ideas { get; private set; }
    public IMongoCollection<Topics> Topics { get; private set; }

    private readonly IMongoDatabase _database;

    public AppDbContext()
    {
        var client = new MongoClient("mongodb://localhost:27017");
        _database = client.GetDatabase("capstone");
        
        Votes = _database.GetCollection<Votes>("votes");
        Ideas = _database.GetCollection<Idea>("ideas");
        Topics = _database.GetCollection<Topics>("topics");
    }
}