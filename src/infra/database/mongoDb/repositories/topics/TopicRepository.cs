﻿using cis.domain.entities;
using MongoDB.Driver;

namespace cis.infra.database.mysql.repositories.topics;

public class TopicRepository : AbstractRepository, ITopicRepository
{
    public void AddTopic(Topics topics)
    {
        AppDbContext.Topics.InsertOne(topics);
    }

    public List<Topics> GetAllTopics()
    {
        return AppDbContext.Topics.Find(_ => true).ToList();
    }

    public Topics GetTopicsById(string id)
    {
        return AppDbContext.Topics.Find(u => u.Id == id).FirstOrDefault();
    }
}