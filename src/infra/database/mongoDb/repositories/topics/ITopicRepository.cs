﻿using cis.domain.entities;
namespace cis.infra.database.mysql.repositories.topics;

public interface ITopicRepository
{
    void AddTopic(Topics topic);
    List<Topics> GetAllTopics();
    Topics GetTopicsById(string id);
}