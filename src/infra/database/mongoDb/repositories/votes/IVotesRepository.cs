﻿using cis.domain.entities;

namespace cis.infra.database.mysql.repositories.votes;

public interface IVotesRepository
{
    void AddVotes(Votes votes);
    
}