﻿using cis.domain.entities;

namespace cis.infra.database.mysql.repositories.votes;

public class VotesRepository : AbstractRepository, IVotesRepository
{
    public void AddVotes(Votes votes)
    {
        AppDbContext.Votes.InsertOne(votes);
    }
}