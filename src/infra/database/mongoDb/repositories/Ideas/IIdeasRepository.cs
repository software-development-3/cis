﻿using cis.domain.entities;

namespace cis.infra.database.mysql.repositories.Ideas;

public interface IIdeasRepository
{
    void AddIdeas(Idea idea);
    List<Idea> GetAllIdeas();
    Idea GetIdeaById(string id);
    void DeleteIdeaById(string id);
    void EditIdea(string id, Idea ideaToEdit);
}