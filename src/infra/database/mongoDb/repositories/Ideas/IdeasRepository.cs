﻿using cis.domain.entities;
using MongoDB.Driver;

namespace cis.infra.database.mysql.repositories.Ideas;

public class IdeasRepository : AbstractRepository, IIdeasRepository
{
    public void AddIdeas(Idea idea)
    {
        AppDbContext.Ideas.InsertOne(idea);
    }

    public List<Idea> GetAllIdeas()
    {
        return AppDbContext.Ideas.Find(_ => true).ToList();
    }

    public Idea GetIdeaById(string id)
    {
        return AppDbContext.Ideas.Find(u => u.Id == id).FirstOrDefault();
    }

    public void DeleteIdeaById(string id)
    {
        AppDbContext.Ideas.DeleteOne(x => x.Id == id);
    }

    public void EditIdea(string id, Idea ideaToEdit)
    {
        if (ideaToEdit == null) throw new ArgumentNullException(nameof(ideaToEdit), "Provided idea is null.");
        
        var idea = AppDbContext.Ideas.Find(idea => idea.Id == id).FirstOrDefault();
        if (idea == null) throw new KeyNotFoundException($"No idea found with ID {id}.");

        if (!string.IsNullOrEmpty(ideaToEdit.Description)) idea.Description = ideaToEdit.Description;
        if (!string.IsNullOrEmpty(ideaToEdit.Title)) idea.Title = ideaToEdit.Title;

        var filter = Builders<Idea>.Filter.Eq("_id", id);
        var replaceResult = AppDbContext.Ideas.ReplaceOne(filter, idea);

        if (replaceResult.IsAcknowledged && replaceResult.ModifiedCount == 0)
        {
            throw new InvalidOperationException($"Failed to update idea with ID {id}.");
        }
    }
}