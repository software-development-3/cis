﻿using System.Security.Claims;

namespace cis.infra.middleware;

public class AuthenticationMiddleware
{
    private readonly RequestDelegate _next;
    private readonly HttpClient _httpClient = new HttpClient();

    public AuthenticationMiddleware(RequestDelegate next)
    {
        _next = next;
    }

    public async Task Invoke(HttpContext context)
    {
        if (context.Request.Headers.TryGetValue("Authorization", out var token))
        {
            token = token.ToString().Replace("Bearer ", "");
            var userLogin = await GetUserLoginByTokenAsync(token!);
            if (!string.IsNullOrEmpty(userLogin))
            {
                var claimsIdentity = new ClaimsIdentity(
                    new[] { new Claim("login", userLogin) }, "Bearer");
                var claimsPrincipal = new ClaimsPrincipal(claimsIdentity);
                context.User = claimsPrincipal;

                await _next(context);
                return;
            }
        }

        context.Response.StatusCode = 401;
        await context.Response.WriteAsync("Unauthorized");
    }

    private async Task<string?> GetUserLoginByTokenAsync(string token)
    {
        try
        {
            var response = await _httpClient.GetAsync($"http://localhost:8080/api/users/verify/{token}");
            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();
                return content;
            }
        }
        catch (Exception ex)
        {
            // Log exception details here, e.g., using ILogger
            Console.WriteLine("Error fetching user login: " + ex.Message);
        }
        return null;
    }
}