﻿namespace cis.infra.middleware;

public class UserLoginInjectionMiddleware
{
    private readonly RequestDelegate _next;

    public UserLoginInjectionMiddleware(RequestDelegate next)
    {
        _next = next;
    }

    public async Task InvokeAsync(HttpContext context)
    {
        if (context.User.Identity != null && context.User.Identity.IsAuthenticated)
        {
            var userLogin = context.User.Claims.FirstOrDefault(c => c.Type == "login")?.Value;
            if (!string.IsNullOrEmpty(userLogin))
            {
                context.Items["login"] = userLogin;
            }
        }
        
        await _next(context);
    }
}