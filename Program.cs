using cis.http.exceptions;
using cis.infra.database.mysql.repositories.Ideas;
using cis.infra.database.mysql.repositories.topics;
using cis.infra.database.mysql.repositories.votes;
using cis.infra.middleware;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddTransient<IIdeasRepository, IdeasRepository>();
builder.Services.AddTransient<IVotesRepository, VotesRepository>();
builder.Services.AddTransient<ITopicRepository, TopicRepository>();
builder.Services.AddExceptionHandler<ErrorHandlingMiddleware>();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Error");
    app.UseHsts();
}

app.UseHttpsRedirection();

app.Use(async (context, next) =>
{
    // Do work that can write to the Response.
    await next.Invoke();
    // Do logging or other work that doesn't write to the Response.
});

app.UseHttpsRedirection();

app.UseRouting();

app.UseAuthorization();

app.UseMiddleware<AuthenticationMiddleware>();
app.UseMiddleware<UserLoginInjectionMiddleware>();

app.MapControllers();

app.Run();